﻿using UnityEngine;
using UnityEngine.Events;

namespace Global {
    public class GameManager : MonoBehaviour {
        public UnityEvent updateLoop;
        public UnityEvent fixedUpdateLoop;
        void Update()
        {
            updateLoop.Invoke();
        }
    
        void FixedUpdate() {
            fixedUpdateLoop.Invoke();
        }
    }
}
