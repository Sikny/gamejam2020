﻿using UnityEngine;
using  UnityEngine.UI;
using  Character;

enum Index
{
    HEAD = 0,
    BELLY = 1,
    LEFT_ARM = 2,
    RIGHT_ARM = 3,
    LEFT_LEG = 4,
    RIGHT_LEG = 5
};
public class EquipmentMenu: MonoBehaviour
{
    [SerializeField]
    private BodyPart[] members;

    [SerializeField]
    private GameObject[] membersLocation;

    [SerializeField]
    private RawImage oldDescriptionPanel;
    [SerializeField]
    private RawImage newDescriptionPanel;

    private int index_members;
    private int a;

    public BodyPart MemberToEquip { get; set; }

    private void Update()
    {
        displayOldDescription(selectMember());
        if (MemberToEquip != null && Input.GetButtonDown("Submit"))
        {
            setMember(MemberToEquip,index_members);
        }
        if (Input.GetButtonDown("delete"))
        {
            throwMember(index_members++);
        } 
    }

    private void Awake()
    {
        index_members = 0;
        foreach (var m in membersLocation){m.SetActive(false);}
    }

    private void throwMember(int index)
    {
        members[index] = null;
    }

    private void setMember(BodyPart member, int index)
    {
        members[index] = member;
    }

    private void displayOldDescription(BodyPart member)
    {
        if (member != null)
            oldDescriptionPanel.GetComponentInChildren<Text>().text = member.description;
    }
    
    private void displayNewDescription(BodyPart member)
    {
        if (member != null) 
            newDescriptionPanel.GetComponentInChildren<Text>().text = member.description;
    }

    private BodyPart selectMember()
    {
        if (Input.GetButtonDown("select_right")) a = (a + 1) % members.Length;
        if (Input.GetButtonDown("select_left")) a  =  (a - 1) % members.Length;
        index_members = Mathf.Abs(a);
        foreach (var m in membersLocation){m.SetActive(false);}
        membersLocation[index_members].SetActive(true);
        return members[index_members];
    }
}
