﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Character {
    public class BodyPart : MonoBehaviour{
        public string partName;
        public string description;
        
        public List<StatModifier> statsModifiers;

        public void DoAction() {
            
        }
    }
}
