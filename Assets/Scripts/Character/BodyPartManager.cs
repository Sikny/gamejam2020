﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Character {
    public class BodyPartManager : MonoBehaviour {
        public List<Transform> bodyPartParents;
        private BodyPart[] _bodyParts;

        private void Awake() {
            _bodyParts = new BodyPart[bodyPartParents.Count];
            int partsCount = bodyPartParents.Count;
            for (int i = 0; i < partsCount; i++) {
                _bodyParts[i] = bodyPartParents[i].GetComponentInChildren<BodyPart>();
            }
        }
        
        public BodyPart ChangePart(int index, BodyPart newPart) {
            if (_bodyParts.Contains(newPart)) return null;
            BodyPart old = _bodyParts[index];
            _bodyParts[index] = newPart;
            if (old != null) {
                old.transform.position = _bodyParts[index].transform.position;
                old.transform.SetParent(_bodyParts[index].transform.parent);
                old.gameObject.layer = 9;
            }
            //Instantiate(_bodyParts[index], bodyPartParents[index]);
            _bodyParts[index].transform.SetParent(bodyPartParents[index]);
            _bodyParts[index].transform.localPosition = Vector3.zero;
            _bodyParts[index].gameObject.layer = 10;
            return old;
        }

        public BodyPart GetPart(int index) {
            return _bodyParts[index];
        }
    }
}
