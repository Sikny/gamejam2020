﻿using System;
using UnityEngine;

namespace Character {
    [Serializable]
    public class StatModifier {
        public string statName;
        public float value;
    }
}
