﻿using System;
using System.Collections;
using System.Threading;
using UnityEngine;

namespace Character {
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerController : MonoBehaviour {
        public Rigidbody rigidBody;
        public float speed = 1f;
        public float lootRadius = 2f;

        private BodyPartManager _bodyPartManager;

        private void Awake() {
            _bodyPartManager = GetComponent<BodyPartManager>();
        }


        public void UpdateLoop() {
            HandleMovement();
            HandleLoot();
        }

        private void HandleMovement() {
            Vector3 movementInput = new Vector3(
                Input.GetAxis("Horizontal"),
                0,
                Input.GetAxis("Vertical")
            ).normalized;
            rigidBody.MovePosition(rigidBody.position+speed*Time.deltaTime*movementInput);
        }

        private void HandleLoot() {
            Collider[] colliderList = Physics.OverlapSphere(transform.position, lootRadius, 1 << 9);
            if (colliderList.Length > 0 && Input.GetKeyDown(KeyCode.F)) {
                _bodyPartManager.ChangePart(0, colliderList[0].GetComponent<BodyPart>());
            }
        }
    }
}


















